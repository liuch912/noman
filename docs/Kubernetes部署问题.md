# Kubernetes 部署问题

## 1. 环境准备问题

目前的系统配置、内核调整暂已满足要求，Kubernetes部署则需要为环境安装所需的依赖。

### 1.1 系统库软件安装

KubeSphere依赖，KubeSphere运行所必须的系统库
```
docker-ce
ipset
conntrack
ebtables
socat
```

常用工具，部署或者日常运维可能会用到的系统库与软件
```
ansible
docker-compose
wget 
unzip
jq
net-tools
vim
telnet
```

### 1.2 二进制文件准备
部署或者日常运维一定会用到的二进制程序
```
kubectl
helm
kubekey
```

## 2. 中间件部署问题

### 2.1 单点故障问题

面对MySQL、MongoDB、ElasticSearch与RabbitMQ，不同组件有它各自的高可用部署模式。在对架构评审要求较高的场景，并且以上中间件需要我方自行部署的话，如何快速、自定义部署至目标环境。

中间件自动化运维问题，对于不同的部署模式，是否存在修复操作、同步操作等等；在与不在Kubernetes上，是否需要通过cronjob或者 Kubernetes Job的方式来完成运维；

### 2.2 哪些中间件需要上Kubernetes，哪些不需要上？如果不上Kubernetes，我们在特定环境上部署中间件，是否会对Kubernetes上组件产生影响？

如果有专门的机器允许仅部署数据库服务器，那其实主要Kubernetes上的工作负载的连接问题即可，可考虑使用Service引入外部服务；

如果要在Kubernetes所在集群节点内部再部署中间件，则还需要考虑资源问题，例如MongoDB内存占用较大，分布式组件如果数据同步会不会抢占大量资源 等等。

如果要在Kubernetes上部署中间件，则需要对其状态做比较多的考虑，对于有状态副本集，我们需要对它们在不同情况下（比如正常情况、扩容、退役节点等）的存储与网络状态，需要做一些调研。

## 3. 自研应用部署问题

### 3.1 license 挂载问题

对于二级节点的注册与解析服务，我们需要先获取设备指纹（通过DigestTool），并提供给顶级，然后拿到license，再挂入Pod内。如何保证在获取license后，设备指纹不会变动，使程序能够稳定地被授权？

需要知道DigestTool所获取的设备指纹，主要受什么因素影响，理论上如果知道，我们总有手段让输出的指纹稳定；比如，如果设备指纹仅受系统网络设备的影响，我们可以使用 host network的模式，进行测试。

目前主要考虑的方向是，结合将设备指纹做成 Secret挂入StatefulSet。

对于企业托管服务与自建企业节点所用到的license，则需要凭借通过dmidecode获得的指纹生成（倒是无需通过顶级），解决问题的思路与上面的license相同，但还需调研与测试 dmidecode 生成设备指纹的大概原理。

license由于对设备指纹的绑定关系，会涉及到注册服务与解析服务对节点的绑定关系。所以需要提前对节点亲和性或者nodeSelector方面做一些考虑。

### 3.2 注册与解析服务的容器镜像与Manifest

我认为首先是二级节点注册与解析服务容器化的过程，之后要考虑转换成Kubernetes的Manifest。我们需要先拥有它们的Dockerfile（可能已经有了），然后是Kubernetes部署需要的 ConfigMap，Service 与 Deployment，如果解析服务（shns）需要落盘数据，如何解决它的状态问题。

需要关注shsrs与shns的进程。

### 3.3 idis-proxy 的运行方式

在了解大概的idis-proxy的用处后，我认为可以通过 sidecar容器 方式将idis-proxy放入 二级节点注册与解析的Pod中。

在使用sidecar容器后，可能需要对整个Pod的生命周期进行考虑，思路大概为对lifecyle block 做比较精细的配置。因为容器组能包含多个容器，需要考虑如何对容器管理编排。

### 3.4 idis-proxy 的运行方式

在了解大概的idis-proxy的用处后，我认为可以通过 sidecar容器 方式将idis-proxy放入 二级节点注册与解析的Pod中。

在使用sidecar容器后，可能需要对整个Pod的生命周期进行考虑，思路大概为对lifecyle block 做比较精细的配置。因为容器组能包含多个容器，需要考虑如何对容器管理编排。

### 3.5 SNMS与log-chart-job(统计分析、数据上报服务的部署)服务拆分

SNMS的snms-server与snms-vue，数据分析与上报的四个容器，其实它们都已经有各自的容器，但需要把他们做成Kubernetes资源，具体要研究这些进程的对资源共享情况，是否需要放入相同容器组。

## 4. 安全性问题

### 4.1 资源分配问题

简单来说就是，如何为Pod分配resource.request与resource.limit，（凭借哪些因素，延迟？或者有没有合适的压测数据？），分配多大。

### 4.2 探针配置问题

主要针对 liveness probe 与 readiness probe，我们通过什么协议、如何判断服务的死活与是否可用。

### 4.3 监控问题

这里暂不考虑业务监控的话，我们如果监控Kubernetes集群本身、节点、中间件、自研应用等等。如果要外接时序数据库，我们需要从哪里获取这些指标。如果需要制定监控规则，我们通过什么来制定metrics？需不需要通过SLA？

尤其Kubernetes本身的一些指标如何获取，需要调研。

## 5. 日志收集

可能需要先在一定程度上了解日志的用途。

### 5.1 日志系统 （EFK）是否需要

EFK对于我们这一套的部署是不是太重，并且日志的格式是否比较好的支持日志系统。

### 5.2 日志存储

对于大量的日志，我们如何存储。如果使用localpv，则如何防止磁盘占用过大，可否考虑sidecar容器处理日志文件 或者 对文件进行压缩。 或者这能不能够从本质上解决问题。