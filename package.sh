#!/bin/bash

echo "==> Downloading packages and artifact ..."
wget https://teleinfo.pek3b.qingstor.com/deploy-packages/ansible.tar.gz -P packages/
wget https://teleinfo.pek3b.qingstor.com/deploy-packages/kubekey-v2.1.0-linux-amd64.tar.gz -P packages/
wget https://teleinfo.pek3b.qingstor.com/deploy-packages/kubesphere.tar.gz -P packages/
wget https://teleinfo.pek3b.qingstor.com/deploy-packages/yum.tar.gz -P packages/

# Package tar ball
echo "==> Packaging ..."
base=$(basename $PWD)
cd ..
tar -czf install.tar.gz $base

echo "==> Done!"

