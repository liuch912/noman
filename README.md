# Noman

`Noman`是基于Bare Metal环境的Kubesphere部署工具.

noman适用于以下三种场景:

* 在离线及在线集群中，部署Kubernetes
* 在离线及在线集群中，同时部署Kubernetes与Kubesphere
* 在离线及在线集群中的已有Kubenetes上，安装Kubesphere

## 设计思路

* 使用 `Kubekey` 部署与看装Kubernetes与Kubesphere。 `Kubekey` 支持多种安装选项，是安装更加简便，节省安装时间。

* 通过 `Ansible` 完成集群各个Bare Metal节点的配置工作，并将整个配置与安装过程自动化。相比较其他自动化运维工具， `Ansible` 有安装简便、配置管理便利、配置语言通俗易学的特点。
  * 将 `ansible.cfg` 单独做进安装包根目录，意味着安装指令须在此目录下执行；但同时较好的“隔离”了工作环境，并有利于标准化的交付。
  * 将 Ansible Inventory 文件 `basae_config.yaml`， 同时也作为全局配置文件，声明式定义了整个系统的最终形态。

* 目前默认使用 `calico` 作为Kubernetes的 CNI 网络插件，在集群模式下相比较flannel，配置与性能的结合对比之下，性价比更高。后续也可变更。
  
* 目前默认使用 `openebs` 作为Kubernetes的 CSI 容器存储插件，并结合业务场景，选择默认使用localpv模式做存储的动态供应。Kubesphere本身有队openebs的集成。

## 环境推荐与需求

### 机器规格

* 测试使用的三节点模式：
  * 4 vCPUs
  * 16 GB RAM
  * 200 GB 存储
* 待定

### 操作系统需求

* CentOS 7 x86_64
* 必须有 `sudo` 权限
* 拥有 `sudo` 权限的用户 须允许`SSH`至集群其他节点
* 集群各节点的时区、时间须同步

### 组件与工具版本推荐

Bare Metal环境最为推荐。我们可以使用noman自动安装所有依赖包。以下为当前测试部署最新版本的Kubesphere时，推荐的组件与工具版本信息：

| Services           | Version | Deployment Tools | Version                      |
| ------------------ | ------- | ---------------- | ---------------------------- |
| Docker             | 20.10.8 | Ansible          | 2.9.27                       |
| Kubernetes         | v1.21.5 | kubectl          | v1.21.5                      |
| KubeSphere         | v3.2.0  | kubekey          | 2.1.0                        |
| CNI                | v0.9.1  | etcd             | RELEASE.2021-06-13T17-48-22Z |
| harbor             | v3.2.1  | helm             | v3.6.3                       |



## 目录结构

完整的离线安装包结构如下图:

```
├── ansible.cfg
├── base_config.yaml
├── logs
│   ├── ansible.log
│   └── kk-install.log
├── packages
│   ├── ansible.tar.gz                        // 离线安装包必须
│   ├── kubekey-v2.1.0-linux-amd64.tar.gz     // 离线安装包必须
│   ├── kubesphere.tar.gz                     // 离线安装包必须，文件可能会比较大
│   ├── manifest.yaml
│   └── yum.tar.gz                            // 离线安装包必须
├── package.sh
├── playbooks
│   ├── bootstrap.yaml
│   ├── install_kubesphere.yaml
│   └── roles
│       ├── docker
│       │   ├── defaults
│       │   │   └── main.yaml
│       │   ├── handlers
│       │   │   └── main.yaml
│       │   ├── tasks
│       │   │   └── main.yaml
│       │   └── templates
│       │       └── daemon.json.j2
│       └── kubesphere
│           ├── tasks
│           │   ├── airgapped_install.yaml
│           │   ├── main.yaml
│           │   └── online_install.yaml
│           └── templates
│               ├── cluster_config_final.yaml.j2
│               ├── cluster_config.yaml.j2
│               └── create_project_harbor.sh.j2
└── README.md
```

## 部署步骤

### 安装包打包

**如果目标集群外网访问良好，可选择跳过此步骤，直接在目标环境中拉取代码**

***1. 拉取代码***

```
$ git clone https://gitlab.com/liuch912/noman.git
```

***2. 下载工具包、依赖包与Kubesphere制品***

```
$ cd noman
$ ./pachage.sh
```

### 部署前的准备

***1. 解压安装包***

目前安装包解压后占用的空间大约为24G，需提前准备好足够的磁盘空间。

```
$ tar zxvf install.tar.gz
```

***2. 安装Ansible***

```
$ cd noman
$ cd packages
$ tar zxvf ansible.tar.gz
$ cd ansible
# ./install.sh
```

如果是是在线部署模式，可执行
```
# apt install ansible
```

***3. 配置 base_config.yaml***

```
all:
  hosts:
    192.168.0.9:
    192.168.0.5:
    192.168.0.4:
#####################
### Global Variables
######################

  vars:
    airgapped_mode: false
    k8s_hosts:
      - ip: 192.168.0.9
        hostname: master
        roles:
        - master
        - etcd
        - worker
      - ip: 192.168.0.5
        hostname: worker1
        roles:
        - etcd
        - worker
      - ip: 192.168.0.4
        hostname: worker2
        roles:
        - etcd
        - worker
    data_dir: "/data"
    runtime_user: "deploy"
    ansible_user: "root"
    ansible_user_password: ""
```
### 部署

***1. 环境初始化 ***

*If we need ssh password to ssh to any other nodes, then we should add `-k` flag behind every ansible or ansible-playbook command line.*

```
$ cd noman
$ ansible-playbook playbooks/bootstrap.yaml 
```

***2. 一键部署 ***

```
$ cd autopilot
$ ansible-playbook playbooks/install_kubesphere.yaml
```

### 删除集群

我们可以使用 `kubekey` 来卸载整个 KubeSphere 与 Kubernetes 集群。

```
$ ./kk delete cluster -f cluster_config_final.yaml
$ docker stop regisry && docker rm registry // If it's air-gapped deployment
$ docker images prune -a  (type 'yes')    // Remove local images if necessary
```

### 检查集群所用的所有镜像

```
kubectl get pods --all-namespaces -o jsonpath="{.items[*].spec.containers[*].image}" |\
tr -s '[[:space:]]' '\n' |\
sort |\
uniq -c
```

### 查看ks-installer的日志

```
kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
```

### 重新构造artifact注意事项

执行kk时，同级目录下的kubekey目录会保存artifact tarball打包前的内容。 尽量少修改kubekey目录下的内容，会造成index.json里元数据的错乱，导致安装时，从artifact中提取镜像layer失败。